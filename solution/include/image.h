#pragma once
#include "errors.h"
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
struct image {
	uint64_t width, height;
	struct pixel* data;
};
struct pixel { uint8_t b, g, r; };
enum read_status init_image(struct image* image, uint32_t const width, uint32_t const height, FILE* const file);
uint64_t calculate_padding(uint64_t const width);
enum write_status write_image(FILE* out, const struct image* image);
void destroy_image(struct image* image);
