#pragma once


#include "errors.h"

enum open_status open_file(const char* file_name, FILE** file, const char* mode);
bool close_file(FILE* file);
