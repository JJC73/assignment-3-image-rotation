
#include "image.h"
struct __attribute__((packed)) bmp_header 
{
    uint16_t bfType;
    uint32_t  bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t  biHeight;
    uint16_t  biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t  biClrImportant;
};



enum read_status from_bmp(FILE* file, struct image* image);
bool read_header(FILE* file, struct bmp_header* header);
enum write_status to_bmp(FILE* out, struct image const* img);
bool write_header(FILE* file, struct bmp_header* header);
struct bmp_header* create_header(struct image const* img);
