#pragma once
#include <stdbool.h>
#include <stdio.h>
enum open_status {
	OPEN_OK = 0,
	OPEN_INVALID_FILE_NAME,
};
enum read_status {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_INCORRECT_MEMORY_ALLOCATION
};
enum  write_status {
    WRITE_OK = 0,
    WRITE_ERROR
};
void print_read_status_error(enum read_status const status);
void print_write_status_error(enum  write_status const status);
void print_open_status_error(enum open_status const status);
