#define _CRT_SECURE_NO_DEPRECATE
#include "reader.h"
enum open_status open_file(const char* file_name, FILE** file, const char* mode) {
	if (!file_name) {
		return OPEN_INVALID_FILE_NAME;
	}
	*file = fopen(file_name, mode);
	if (*file==NULL) {
		return OPEN_INVALID_FILE_NAME;
	}
	return OPEN_OK;
}
bool close_file(FILE* file) {
	return fclose(file);
}
