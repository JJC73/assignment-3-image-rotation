#include "image.h"
enum read_status init_image(struct image* image, uint32_t const width, uint32_t const height, FILE* const file) {
	struct pixel* data = malloc(sizeof(struct pixel) * height * width);
	if (!data) {
		free(data);
		return READ_INCORRECT_MEMORY_ALLOCATION;
	}
	if (!image) {
		free(data);
		free(image);
		return READ_INCORRECT_MEMORY_ALLOCATION;
	}
	
	uint64_t padding = calculate_padding(width);
	for (size_t h = 0; h < height; h++) {
		for (size_t w = 0; w < width; w++) {
			if (!fread(data + h * width + w, sizeof(struct pixel), 1, file)) {
				return READ_INVALID_BITS;
			}
		}
		if (fseek(file, (uint32_t)padding, SEEK_CUR)) {
			return READ_INVALID_BITS;
		}

	}
	
	image->height = height;
	image->width = width;
	image->data = data;
	return READ_OK;
}

uint64_t calculate_padding(uint64_t const width) {
	return (4-(uint64_t)(width*sizeof(struct pixel))%4)%4;
}

enum write_status write_image(FILE* out, const struct image* image) {
	for (size_t h = 0; h < image->height; h++) {
		for (size_t w = 0; w < image->width; w++) {
			if (!fwrite(image->data + h * image->width + w, sizeof(struct pixel), 1, out)) {
				return WRITE_ERROR;
			}
		}
		if (fseek(out, (uint32_t)calculate_padding(image->width), SEEK_CUR)) {
			return WRITE_ERROR;
		}
	}
	return WRITE_OK;
}
void destroy_image(struct image* image) {
	free(image->data);
	free(image);
}
