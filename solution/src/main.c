#include "bmp.h"
#include "image.h"
#include "reader.h"
#include "rotate.h"
int main(int argc, char** argv){
	if (argc != 3) {
		printf("Incorrect number of arguments");
		return -1;
	}
	FILE* input_file=NULL;
	FILE* output_file=NULL;
	enum open_status open_input_file_status = open_file(argv[1], &input_file,"rb");
	enum open_status open_output_file_status = open_file(argv[2], &output_file, "wb");
	if (open_input_file_status==OPEN_INVALID_FILE_NAME) {
		printf("%s" "%s" "%s", "File with name ", argv[1], " not found");
		return -1;
	}
	if (open_output_file_status == OPEN_INVALID_FILE_NAME) {
		printf("%s" "%s" "%s", "File with name ", argv[2], " not found");
		return -1;
	}
	struct image* input_image =  malloc(sizeof(struct image));
	
	enum read_status read_status = from_bmp(input_file, input_image);
	if (read_status != READ_OK) {
		print_read_status_error(read_status);
		return -1;
	}
	struct image output_image = { 0 };
	output_image =rotate(*input_image);
	enum write_status write_status = to_bmp(output_file ,&output_image);
	if (write_status != WRITE_OK) {
		print_write_status_error(write_status);
		return -1;
	}
	destroy_image(input_image);
	free((&output_image)->data);
	if (close_file(input_file)) {
		printf("Error: closing is incorrect");
		return -1;
	}
	if (close_file(output_file)) {
		printf("Error: closing is incorrect");
		return -1;
	}
	return 0;
}

