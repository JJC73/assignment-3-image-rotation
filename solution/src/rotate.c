#include "rotate.h"
struct image rotate(struct image const source) {
	struct image output_image = {
	.height = source.width,
	.width = source.height
	};
	struct pixel* data = malloc(sizeof(struct pixel) * source.width * source.height);
	for (size_t h2_i = 0; h2_i < output_image.height; h2_i++) {
		for (size_t w2_i = 0; w2_i < output_image.width; w2_i++) {
			*(data + h2_i * (output_image.width) + w2_i) = *(source.data + (source.height - 1 - w2_i) * source.width + h2_i);
		}
	}
	output_image.data = data;
	return output_image;
}
