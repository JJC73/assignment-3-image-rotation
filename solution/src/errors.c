#include "errors.h"

void print_read_status_error(enum read_status const status) {
    switch (status) {
    case READ_INVALID_SIGNATURE:
        printf("Error: Incorrect signature ");
        break;
    case READ_INVALID_BITS:
        printf("Error: reading the bits is incrorrect");
        break;
    case READ_INVALID_HEADER:
        printf("Error: reading the header is incrorrect");
        break;
    case READ_INCORRECT_MEMORY_ALLOCATION:
        printf("Error: Incorrect memory allocation");
        break;
    default:
        printf("%s", "Error: reading is incrorrect");
        break;
    }
}
void print_write_status_error(enum  write_status const status) {
    switch (status)
    {
    case WRITE_ERROR:
        printf("Error: writing to the file is incrorrect ");
        break;
    default:
        printf("Error: writing is incrorrect");
        break;
    }
}
void print_open_status_error(enum open_status const status) {
    switch (status)
    {
    case OPEN_INVALID_FILE_NAME:
        printf("Error: file not found");
        break;
    default:
        printf("Error with open");
        break;
    }
}
