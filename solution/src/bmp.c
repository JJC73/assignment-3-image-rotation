#include "bmp.h"
#define BMP_TYPE 0x4D42
#define RESERVED_BYTES 0
#define PLANES 1
#define BIT_COUNT 24
#define HEADER_SIZE 40
#define X_AND_Y_PER_METER 0
#define NUMBER_OF_COLOR_INDEX 0
#define COMPRESSION 0
enum read_status from_bmp(FILE* file, struct image* image) {
	struct bmp_header header = {0};
	if (!read_header(file, &header)) {
		return READ_INVALID_HEADER;
	}
	return init_image(image, header.biWidth, header.biHeight, file);
}

bool read_header(FILE* file, struct bmp_header* header) {
	return fread(header, sizeof(struct bmp_header), 1, file);
}
bool write_header(FILE* file, struct bmp_header* header) {
	return fwrite(header, sizeof(struct bmp_header), 1, file);
}
struct bmp_header* create_header(struct image const* img) {
	uint32_t size = (img->height * img->width) * sizeof(struct pixel) + img->height * calculate_padding(img->width);
	struct bmp_header* header = malloc(sizeof(struct bmp_header));
	header->bfType = BMP_TYPE;
	header->bfileSize = sizeof(struct bmp_header) + size;
	header->bfReserved = RESERVED_BYTES;
	header->bOffBits = sizeof(struct bmp_header);
	header->biSize = HEADER_SIZE;
	header->biWidth = img->width;
	header->biHeight = img->height;
	header->biPlanes = PLANES;
	header->biBitCount = BIT_COUNT;
	header->biCompression = COMPRESSION;
	header->biSizeImage = size;
	header->biXPelsPerMeter = X_AND_Y_PER_METER;
	header->biYPelsPerMeter = X_AND_Y_PER_METER;
	header->biClrUsed = NUMBER_OF_COLOR_INDEX;
	header->biClrImportant = NUMBER_OF_COLOR_INDEX;
	return header;
}


enum write_status to_bmp(FILE* out, struct image const* img) {
	struct bmp_header* header = create_header(img);
	if (!write_header(out, header)) {
		free(header);
		return WRITE_ERROR;
	}
	free(header);
	return write_image(out, img);
}
